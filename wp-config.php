<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bitbucket_demo');

/** MySQL database username */
define('DB_USER', 'bitbucket_demo');

/** MySQL database password */
define('DB_PASSWORD', 'bitbucket_demo');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '#(Y~{Nz%BKbv^-<FGN2(rULx([$F0RhFB/c&>*QS|a{Cbp/K@2aUPp@cU&,Z!tVa');
define('SECURE_AUTH_KEY',  'EOqW6<f=|nv8+w}/4I2Ef-&{Oo^/Q-ul)9b%]>>/=!G&&&gE5i)G-.A]:0:wnhl>');
define('LOGGED_IN_KEY',    'QKy*wbi1W*mjXD+Y6_83Hk?,T#|Z})W[9o-3sti{|m,aNxafJ(iO|KP4,X|sXH0r');
define('NONCE_KEY',        ')Pa1jFv:r5ygq`bvb,2B/zBe6l]-N)-Ate(:y_Va2Z@9uZ9$|7v=ETe8|C.Xi]%x');
define('AUTH_SALT',        'kQ>o#yXX95,8tdVsDS//].(kZoRDvb-?r&[]1j?>9c^/!9b ;Cg]I;}z-8c?MGM8');
define('SECURE_AUTH_SALT', 'PMdQN+N]pHJG5-RwhCrlhCi.pVW|:,A}+*a5L6Ea]%JBoa_py6XN6Wc`X7#Z&1D*');
define('LOGGED_IN_SALT',   'WvsRHoD b7|[R0U%r9O9?1IFjk+)#7@r|DQz1Xjyjyd+*pcXde27|iKd-I7&Y3tW');
define('NONCE_SALT',       'In.Muu!?8<V5M:QO+2]TZ2|w+sg<axEh+Hz9Y~q(_%]_n5D)~?tVd^.lBb6^2yqd');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
//This is a test of deploybot
  